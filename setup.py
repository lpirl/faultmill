from os.path import join as path_join, dirname
from setuptools import setup, find_packages

def main():
    version = '0.1'
    README = path_join(dirname(__file__), 'README.rst')
    long_description = open(README).read()
    setup(
        name='faultmill',
        version='0.1',
        description='fault trees in; executable fault injection campaign out',
        long_description=long_description,
        classifiers=[
            'Development Status :: 4 - Beta',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Programming Language :: Python',
        ],
        keywords='fault-injection',
        author='Lukas Pirl',
        author_email='faultmill@lukas-pirl.de',
        url='https://gitlab.com/lpirl/faultmill',
        download_url='https://gitlab.com/lpirl/faultmill/-/archive/master/faultmill-master.tar.bz2',
        package_dir={'': 'src'},
        packages=find_packages('src', exclude=['test']),
        install_requires = [
            'pygraphml',
            'pygraphviz',
        ],
        extras_require={
            'dev': [
                'pylint',
            ],
            'doc': [
                'pylint', # pyreverse
                'Sphinx'
            ]
        },
        entry_points={
            'console_scripts': ['faultmill = faultmill:standalone']
        },
    )


if __name__ == '__main__':
    main()
