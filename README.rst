faultmill
=========

*faultmill* is a tool for identifying and (optionally) running
dependability stress tests.

faultmill derives the combination of all possible worst-case scenarios
(i.e., the dependability stress test) from a user-provided
dependability model.
The resulting fault injection campaign can then be automatically
exercised using user-provided implementations (so-called *workers*).

Users (e.g. developers of fault-tolerant systems) can provide fault
trees in Graphviz DOT language (adhering to naming conventions, see
examples) or a XML format as exported by the `Open Reliability Editor
<https://ore-project.org/>`__. The implementations (to prepare the
system under tests, inject faults, measure quality of service under
presence of faults, remove injected faults, roll the system back, etc.)
need to be provided as executable files.

trivia
------

Modern software systems have reached a level
of complexity where software bugs and hardware failures are
no longer exceptional but a permanent operational threat.
Considering that software systems are increasingly critical, their
dependability properties need to be a first-class concern.

faultmill - with its underlying concepts – presents a structured
approach for *dependability stress testing*. It automatically
determines and injects all variations of the maximum amount of
simultaneous non-fatal errors. This puts the implemented fault
tolerance mechanisms under maximum (fault) load, so that they are
tested for their effectiveness in corner cases.

The prerequisites are a failure space dependability model of the system
under test, as well as executables to interact with the system under
test (e.g., to inject faults). faultmill's deterministic algorithm
programmatically derives fault injection campaigns that maximize
dependability stress, which are then exercised using the aforementioned
executables.

This paves the way not only for throughout dependability assessments,
but also for their integration in continuous development practices.

For further information, see also

* Lena Feinbube, Lukas Pirl, Peter Tröger, and Andreas Polze.
  "Dependability Stress Testing of Cloud Infrastructures." In 2017 18th
  International Conference on Parallel and Distributed Computing,
  Applications and Technologies (PDCAT), pp. 453-460. IEEE, 2017.

using faultmill
---------------

* make sure you have the `APT requirements <apt_requirements.txt>`__ available
  on your system:

  ``apt install $(cat apt_requirements.txt)``

* optionally, but recommended:
  setup a development environment using ``virtualenvwrapper``:

  #. create virtualenv: ``$ mkvirtualenv -p $(type python3 | cut -d " " -f3) faultmill``

     * (automatically enters the environment)

  #. enter virtualenv: ``$ workon faultmill``
  #. leave virtualenv: ``$ deactivate``

* clone this repository:
  ``$ git clone --recursive https://gitlab.com/lpirl/faultmill.git``
* ``$ cd faultmill``
* install Python dependencies: ``$ pip3 install .``
* if you want a quick smoke test, you can run
  ``./faultmill dot test/graphs/dot/vote_2-of-3.dot``

  * the dot graph contains a fault tree which represents a
    "two-out-of-three-must-survive redundancy"
  * accordingly, faultmill will output a campaign with three scenarios:
    one per tolerable fault combination

* if you are interested in a less synthetic example,
  please see `the example directory <example>`__

development / documentation
---------------------------

If you want to extend faultmill, for example, you might want to browse the
`documentation <https://lpirl.gitlab.io/faultmill/>`__.

Enhancing and extending the documentation is as welcome as enhancing and
extending the code.

.. image:: https://gitlab.com/lpirl/faultmill/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/faultmill/pipelines
