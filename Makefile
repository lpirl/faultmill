DOT_SRCS=$(shell find -name '*.dot')
DOT_IMGS=$(DOT_SRCS:.dot=.dot.svg)

FAULTMILL ?= ./faultmill

all_test_graphs:
	-find test -name '*.dot' $(FIND_EXAMPLES_EXTRA_ARGS) \
	| xargs -tL1 ./faultmill dot

	-find test -name '*.xml' $(FIND_EXAMPLES_EXTRA_ARGS) \
	| xargs -tL1 ./faultmill FuzzEdGraphMl

all_valid_test_graphs: FIND_EXAMPLES_EXTRA_ARGS = -not -name 'invalid-*'
all_valid_test_graphs: all_test_graphs

.PHONY: dots_to_svgs
dots_to_svgs: $(DOT_IMGS)

%.dot.svg: %.dot
	@type dot > /dev/null || (\
		echo "Please (apt-get/aptitude/yum/…) install graphviz" && exit 1 \
	)
	dot -Tsvg $^ > $@

lint:
	cd src && pylint --rcfile=../.pylintrc faultmill

.PHONY: doc
doc: doc/source/classes.dot.svg
	sphinx-apidoc -o doc/source/apidoc -M src/faultmill
	$(MAKE) -C doc html -e

doc-clean:
	find doc -name '*.dot.svg' -delete
	rm -rf doc/source/apidoc/*
	$(MAKE) -C doc clean -

doc/source/classes.dot:
	cd src && pyreverse -ko dot faultmill
	mv src/classes.dot src/packages.dot doc/source
	sed -i "s/rankdir=BT/rankdir=RL\noverlap=false/" $@

clean: doc-clean
	find -name __pycache__ -or -name '*.pyc' -delete
	rm -f $(DOT_IMGS)
