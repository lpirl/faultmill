#!/bin/sh -e

. `dirname "$0"`"/common-vars.sh"

scenario=$1
shift

echo "$(date +%s.%N);$scenario;$@" >> "$TMP_ROOT/log"
