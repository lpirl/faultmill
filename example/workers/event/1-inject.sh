#!/bin/sh -e

. `dirname "$0"`"/../common-vars.sh"

case "$2" in
  "basic_backup_0_corrupt")
    # injected fault: backup file 0 gets lost:
    rm "$TMP_DIR/myfile-backup-0"
    ;;
  "basic_backup_1_corrupt")
    # injected fault: backup file 1 becomes inaccessible
    chmod -x "$TMP_DIR/myfile-backup-1"
    ;;
  "basic_backup_2_corrupt")
    # injected fault: backup file 2 gets modified
    echo "\0" >> "$TMP_DIR/myfile-backup-2"
    ;;
  "basic_native_restore_faulty")
    # injected fault: super-backup ends up in an infinite loop
    echo -n > "$TMP_DIR/super-backup"
    echo '#!/usr/bin/env python' >> "$TMP_DIR/super-backup"
    echo 'while True:' >> "$TMP_DIR/super-backup"
    echo '  pass' >> "$TMP_DIR/super-backup"
    ;;
  "basic_rescue_restore_faulty")
    # injected fault: the rescue script gets lost
    rm "$TMP_DIR/myfile-restore.sh"
    ;;
  *)
    echo "don't know how to inject '$2'"
    exit 1
esac
