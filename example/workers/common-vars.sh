#!/bin/sh -e

if [ -z ${_COMMON_SH_SOURCED+x} ]
then
  _COMMON_SH_SOURCED=y
  TMP_ROOT="/tmp/faultmill-$USER-$(readlink -f . | md5sum | cut -b 1-4)"
fi

TMP_DIR="$TMP_ROOT/$1"
