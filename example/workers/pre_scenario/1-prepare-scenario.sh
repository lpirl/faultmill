#!/bin/sh -e

here=`dirname "$0"`

. "$here/../common-vars.sh"

if [ -d "$TMP_DIR" ]
then
  echo "'$TMP_DIR' already exists – aborting to avoid data loss"
  exit 1
fi

mkdir "$TMP_DIR"

cp "$here/../../super-backup" "$TMP_DIR/"

# get the file (full of valuable zero bytes) to backup
dd if=/dev/zero of="$TMP_DIR/myfile" bs=1M count=1 status=none

(cd "$TMP_DIR" && ./super-backup backup myfile)

# we delete the original, because we want to test the restore process
rm "$TMP_DIR/myfile"
