#!/bin/sh -e

. `dirname "$0"`"/../common-vars.sh"

[ -z ${TMP_ROOT+x} ] && exit 1
[ "$TMP_ROOT" = "/" ] && exit 1

if [ -d "$TMP_ROOT" ]
then
  echo "'$TMP_ROOT' already exists – aborting to avoid data loss"
  exit 1
fi

mkdir -p "$TMP_ROOT"
