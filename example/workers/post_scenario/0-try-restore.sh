#!/bin/sh -e

here=`dirname "$0"`

. "$here/../common-vars.sh"


restore_was_successful() {
  [ -f "$TMP_DIR/myfile" ] || return 1
  identical_count=$( \
    md5sum "$TMP_DIR/myfile"* 2>/dev/null \
    | sort \
    | uniq -cw 32 \
    | grep 'myfile$' \
    | awk '{print $1}' \
  )
  test $identical_count -ge 3
}

# try the "native", then the "rescue" restore
(cd "$TMP_DIR" \
  && timeout 3s ./super-backup restore myfile 2>/dev/null \
  || true)
if restore_was_successful
then
  "$here/../log.sh" "$1" "restore using 'super-backup' successful"
  exit 0
else
  "$here/../log.sh" "$1" "restore using 'super-backup' unsuccessful"
fi

# if that failed, try the "rescue" restore
(cd "$TMP_DIR" \
  && timeout 3s ./myfile-restore.sh 2>/dev/null \
  || true)
if restore_was_successful
then
  "$here/../log.sh" "$1" "restore using 'myfile-restore.sh' successful"
else
  "$here/../log.sh" "$1" "restore using 'myfile-restore.sh' unsuccessful"
fi
