The files in this directory are supposed to give an idea on
how faultmill can be used and what it does.

We have programmed an example application and we want to assess its dependability.

the system under test: a trivial "backup system"
================================================

As an example application, a (trivial) "backup system" has been
implemented. We can only assess this application, if we know which
behavior to expect. Hence, we need some sort of specification. The
"backup system" should do the following:

* the application should be an executable file called ``./super-backup``
* calling ``./super-backup backup <file>`` should backup the specified
  file

  * three copies of the specified file should be created
  * an additional script to restore the file should be placed next to
    the backup files

    * in case the ``./super-backup`` application gets lost, in case
      the ``./super-backup`` application changes its file format and
      cannot restore the backup, etc.

* ``./super-backup restore <file>`` should restore the specified file
  from the previously created copies
* a corruption of one (out of the three) backup file(s) should be
  tolerated

  * corruptions might be inaccessibility of the file, modified
    contents, etc.

dependability model
===================

From the informal specification above, we derive the following
dependability model:

.. image:: https://gitlab.com/lpirl/faultmill/-/jobs/artifacts/master/raw/example/fault-tree.dot.svg?job=svgs_from_dots
  :width: 100%

Let's quickly go through the dependability model (and a bit through the
fault tree logic). First, note that this graph does not adhere to the
graphical fault tree syntax, since we misuse the Graphviz *dot*
language as one way to formulate fault trees. ¹

We see that there are two main causes which potentially lead to the
inability to restore the backup (i.e., the failure). One reason would
be, that too many backup files are corrupt. This, in turn, is the case
if two or more backup files are corrupt. Hence, the *voting or* gate is
configured to activate (think "fire a signal up the tree") when two or
more (hence ``n=2``) attached events are activated. On the other hand,
the executable files to restore the backup might be absent, broken,
etc. This is the case, if the "native" way to restore the backup
(``./super-backup restore …``) and the fallback way to restore the
backup (``…-restore.sh``) fail.

From this dependability model, faultmill can derive the fault injection
campaign to exercise in order to put our "backup system" under maximum
dependability stress::

  $ faultmill dot example/fault-tree.dot
  Campaign c36b with 6 scenarios
    Scenario 2055
      basic_backup_0_corrupt
      basic_rescue_restore_faulty
    Scenario 7aac
      basic_native_restore_faulty
      basic_backup_2_corrupt
    Scenario cc7d
      basic_backup_1_corrupt
      basic_rescue_restore_faulty
    Scenario bbca
      basic_backup_2_corrupt
      basic_rescue_restore_faulty
    Scenario d056
      basic_native_restore_faulty
      basic_backup_0_corrupt
    Scenario bb6f
      basic_native_restore_faulty
      basic_backup_1_corrupt

¹ As the output indicates, we use a naming convention to express fault
trees in dot.

We can see that the fault injection campaign created by faultmill has
six scenarios. Since our "backup system" should tolerate one corrupt
backup file and one broken restore program, the campaign contains all
those possible combinations
(file 1 + ``./super-backup restore …`` broken,
file 1 + ``…-restore.sh`` broken,
file 2 + ``./super-backup restore …`` broken,
…).

workers
=======

Now, we want faultmill to exercise the campaign above in interaction
with our "backup system". For this, we need so-called "workers". In
this example, there are the following files/directories provided::

  workers/pre_campaign
  workers/pre_scenario
  workers/event
  workers/post_scenario
  workers/post_campaign

faultmill will call these executable files (or the executable files
within the directories, if those paths are directories) to interact with
our "backup system".

Please find a quick overview of the call sequence and the
responsibilities of the respective workers below.

.. image:: https://gitlab.com/lpirl/faultmill/-/jobs/artifacts/master/raw/example/workers.dot.svg?job=svgs_from_dots
  :width: 100%

For implementation details, please read through the files in the
`workers directory <workers>`__. If you do, you'll find out that we inject
different sorts of faults to corrupt the backup files. "Normally"
(i.e., when this would be a serious dependability assessment), it would
be more correct to add those details to the fault tree. Specifically,
the basic events which now represent corrupted backup files would
become intermediate events, each would get an *or* gate attached, and
the *or* gates would get a basic event for each way to corrupt a file
attached (e.g., delete, make unreadable, modify the file's content).

If we run the example, we should see the following output::

  $ faultmill --verbose --workers example/workers dot example/fault-tree.dot
  INFO::running Campaign c36b
  INFO::6 of 6 (100%) scenario runs left to do in total
  INFO::running Scenario bb6f
  INFO::Scenario bb6f: runs left: 0, retries left: 0
  INFO::5 of 6 (83%) scenario runs left to do in total
  INFO::running Scenario cc7d
  INFO::Scenario cc7d: runs left: 0, retries left: 0
  INFO::4 of 6 (66%) scenario runs left to do in total
  INFO::running Scenario d056
  INFO::Scenario d056: runs left: 0, retries left: 0
  INFO::3 of 6 (50%) scenario runs left to do in total
  INFO::running Scenario bbca
  INFO::Scenario bbca: runs left: 0, retries left: 0
  INFO::2 of 6 (33%) scenario runs left to do in total
  INFO::running Scenario 2055
  INFO::Scenario 2055: runs left: 0, retries left: 0
  INFO::1 of 6 (16%) scenario runs left to do in total
  INFO::running Scenario 7aac
  INFO::Scenario 7aac: runs left: 0, retries left: 0
  please see '/tmp/faultmill-lukas-b9d1/' for results / resulted files
  INFO::finished running Campaign c36b

and if we check the log::

  $ cat /tmp/faultmill-…-b9d1/log
  1583944548.526275100;;campaign begin
  1583944548.532220579;bb6f;scenario begin
  1583944551.599764415;bb6f;restore using 'super-backup' unsuccessful
  1583944551.630112872;bb6f;restore using 'myfile-restore.sh' successful
  1583944551.636614884;bb6f;scenario end
  1583944551.646992109;cc7d;scenario begin
  1583944551.754209442;cc7d;restore using 'super-backup' successful
  1583944551.759955881;cc7d;scenario end
  1583944551.769075438;d056;scenario begin
  1583944554.832383292;d056;restore using 'super-backup' unsuccessful
  1583944554.855557214;d056;restore using 'myfile-restore.sh' successful
  1583944554.862594669;d056;scenario end
  1583944554.872425788;bbca;scenario begin
  1583944554.976485602;bbca;restore using 'super-backup' successful
  1583944554.982502202;bbca;scenario end
  1583944554.990726699;2055;scenario begin
  1583944555.097332294;2055;restore using 'super-backup' successful
  1583944555.103114517;2055;scenario end
  1583944555.113851219;7aac;scenario begin
  1583944558.175339415;7aac;restore using 'super-backup' unsuccessful
  1583944558.197064572;7aac;restore using 'myfile-restore.sh' successful
  1583944558.203051775;7aac;scenario end
  1583944558.214050572;;campaign end

we see that our "backup system" successfully withstood all fault
scenarios; i.e., it could always restore the backup.
