Welcome to faultmill's API documentation!
=========================================

This is the developer documentation of faultmill (for a quick general
description of the tool and a guide on how to use it, see the
`repository's README <https://gitlab.com/lpirl/faultmill>`__).

.. toctree::
  :maxdepth: 2
  :glob:

  apidoc/*

class hierarchy
---------------

.. image:: classes.dot.svg
   :width: 750px
   :align: center
