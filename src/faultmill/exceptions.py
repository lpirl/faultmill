"""
A module containing custom exceptions.
"""

class TreeError(Exception):
  """ Raised when input graph violates tree fault invariants etc. """
