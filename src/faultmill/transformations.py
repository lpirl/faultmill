"""
This module contains transformations that can be applied in a
boolean-like logic.

In our data model, this means that it can accordingly transform one set
of sets (i.e. sets of combinations of boolean symbols that are "True")
into another.
The knowledge about this data structure is the main differentiation
between transformations and the ``sets.SetHelpersMixin``.

Those transformations are abstracted to classes/objects since some of
them evolve early in the analysis of a fault tree, but are applied
later/ towards the end of the analyses. Using objects, transformations
can be stored (stateful) and applied whenever adequate.
"""

from abc import ABCMeta, abstractmethod

from faultmill.sets import Set, StaticSet


# Yes, this interface is slim and to achieve the same functionality, we
# could probably use single functions. But in order to leverage the
# functionality provided by the "non-public" functions ``__repr__`` and
# ``__str__``, we use classes nevertheless. Hence, sub-classes will have
# only a single public function, but that's okay.
class AbstractBaseTransformation(metaclass=ABCMeta):
  """
  Common superclass for all transformations.
  """

  @abstractmethod
  def transform(self, in_sets):
    """
    Actually transforms ``in_sets`` and returns the result accordingly.
    Required to be idempotent.
    """

  def __repr__(self):
    return f"{self.__class__.__name__}()"

  def __str__(self):
    return self.__repr__()


# see comment above ``AbstractBaseTransformation``
# pylint: disable=too-few-public-methods
class Minimization(AbstractBaseTransformation):
  """
  Returns sets that are not a superset of any other set.
  """

  def transform(self, in_sets):
    return StaticSet(
      candidate_set for candidate_set in in_sets
      if not candidate_set.issuperset_any(in_sets, True)
    )


# see comment above ``AbstractBaseTransformation``
# pylint: disable=too-few-public-methods
class Maximization(AbstractBaseTransformation):
  """
  Returns sets that are not a subset of any other set.
  """

  def transform(self, in_sets):
    return StaticSet(
      candidate_set for candidate_set in in_sets
      if not candidate_set.issubset_any(in_sets, True)
    )


class ImplicationRedundancyRemoval(AbstractBaseTransformation):
  """
  If all implying and implied symbols are within a set of events, the
  implied symbols are removed since they are redundant.
  """

  def __init__(self, implying_events, implied_events):
    self.implying_events = implying_events
    self.implied_events = implied_events

  def __hash__(self):
    """
    Two ``ImplicationRedundancyRemoval`` are equal (e.g. in sets), if
    they have the same set of ``implying_events`` and ``implied_events``.
    """
    out = id(self.__class__)
    for event_sets in (self.implying_events, self.implied_events):
      for event_set in event_sets:
        out ^= id(event_set)
    return out

  def __eq__(self, other):
    """
    See ``self.__hash__``. This should be present if ``__hash__`` is.
    """
    return hash(self) == hash(other)

  def transform(self, in_sets):

    # shortcuts for convenience and micro-optimization :)
    implied = self.implied_events
    contains_implying = self.implying_events.issubset
    out_set = Set()

    for events in in_sets:
      if contains_implying(events):
        # we do not use events.difference[_update] here, because it
        # returns a [frozen]set instead of [Static]Set, so we would have
        # to re-initialize the whole thing as [Static]Set and would
        # thereby waste the initialization of the intermediate
        # [frozen]set
        events = StaticSet(event for event in events
                           if event not in implied)
      out_set.add(events)

    return out_set

  def __repr__(self):
    return (f"{self.__class__.__name__}"
            f"({self.implying_events} -> {self.implied_events})")
