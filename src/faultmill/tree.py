"""
This module contains our abstraction of a fault tree,
see :class:`FaultTree`.
"""

from itertools import chain
from logging import debug

import faultmill.tree_elements
from faultmill.exceptions import TreeError
from faultmill.sets import StaticSet
from faultmill.transformations import Minimization, Maximization


class FaultTree:
  """
  Representation of a fault tree, including methods to construct them
  and obtain their logical content (i.e., obtain the Boolean relationships
  between the elements).

  This includes trivial helpers (e.g., to obtain all basic events);
  well-known operations (e.g., to obtain all so-called "mincuts");
  as well as our novel algorithm to obtain so-called "maximum injection
  sets". See method documentation for more information.
  """

  def __init__(self, source):
    """
    Constructs a fault tree from ``source``, which is an instance of
    ``GraphSource``.

    In contrast to ``GraphSource`` however, the construction happening
    here will (use elements which will) be aware of fault tree
    specifics (e.g., basic events can have only one parent at max).
    """
    debug("building %r", self)

    if not source.directed:
      raise TreeError(f"{source} is not directed.")

    # instantiate nodes using their corresponding classes:
    nodes = {node_id: source.initialized_node(node_id)
             for node_id in source.node_ids_iter()}

    # wire up the nodes
    for source_node_id, destination_node_id in source.edge_tuples_iter():
      debug("processing edge %s -> %s",
            source_node_id, destination_node_id)
      source_node = nodes[source_node_id]
      destination_node = nodes[destination_node_id]

      source_node.wire_child(destination_node)

    self.root = nodes[source.root_node_id()]
    self.nodes = nodes

  def apply_transformations(self, event_sets):
    """
    Applies all transformations provided by tree elements/nodes again
    and again until no transformation causes any change anymore.
    """
    transformations = StaticSet(chain.from_iterable(
      node.transformations() for node in self.nodes.values()
    ))
    debug("applying TreeElements' transformations: %s", transformations)
    event_sets_pre_transformations = None
    while event_sets != event_sets_pre_transformations:
      event_sets_pre_transformations = event_sets
      for transformation in transformations:
        event_sets = transformation.transform(event_sets)
    debug("event_sets after TreeElements' transformations: %s", event_sets)
    return event_sets

  def mincuts(self):
    """
    Returns the mincuts for this tree.
    They are calculated by applying rules to simplify the the roots'
    cutsets (see also ``TreeElement.cutsets()``).
    """
    cutsets = self.root.cutsets()

    debug("eliminating non-minimal cutsets")
    mincuts = Minimization().transform(cutsets)

    debug("determined mincuts: %s", mincuts)
    return mincuts

  def basic_events(self):
    """
    Returns the set of basic events present in this fault tree.
    """
    basic_events = StaticSet(
      node for node in self.nodes.values()
      if isinstance(node, faultmill.tree_elements.BasicEvent)
    )
    debug("determined basic events: %s", basic_events)
    return basic_events

  def injection_sets(self):
    """
    Returns all sets of basic events for which activating all the basic
    events would not lead to a failure.

    In other words: returns all combinations of faults which – as
    defined by this fault tree – should be tolerated and not lead to a
    failure.
    """
    mincuts = self.mincuts()
    basic_events = self.basic_events()

    debug("determining injection sets")
    injection_sets = StaticSet(
      candidate_set
      for candidate_set in basic_events.powerset_iter()
      if candidate_set and not candidate_set.issuperset_any(mincuts)
    )

    injection_sets = self.apply_transformations(injection_sets)

    debug("determined injection sets: %s", injection_sets)
    return injection_sets

  def maximum_injection_sets(self):
    """
    Returns ``injection_sets`` (see corresponding method) which are not
    a subset of any other of the sets.

    In other words: returns combinations of faults which should be
    tolerated (as defined by this fault tree), so not lead to a
    failure, *and* which are not contained by any other combination of
    faults.

    This results in a minimal number of sets of faults, while still
    considering all fault scenarios (i.e., full "fault coverage").
    Since this implies, that the system under test is confronted with
    worst case fault loads, we refer to this as "dependability stress".
    """
    debug("determining maximum injection sets")
    all_injection_sets = self.injection_sets()
    maximum_injection_sets = Maximization().transform(all_injection_sets)
    maximum_injection_sets = self.apply_transformations(
      maximum_injection_sets
    )
    debug("determined maximum injection sets: %s", maximum_injection_sets)
    return maximum_injection_sets
