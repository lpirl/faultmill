"""
This module contains some nuts and bolts which haven't found a place in
any specific module yet.
"""

from logging import getLogger, debug, DEBUG
from os import access, X_OK
from os.path import isdir
from re import compile as re_compile, IGNORECASE
from subprocess import run


def re_compile_ci(regexp):
  """ Case-insensitively compile a regular expression. """
  return re_compile(regexp, IGNORECASE)


def check_executable(*paths):
  """
  Raises ``RuntimeError`` if any of the provided ``paths`` is neither a
  directory, nor an executable file.
  """
  for path in paths:
    if isdir(path):
      continue
    if access(path, X_OK):
      continue
    raise RuntimeError(
      f"Neither a directory, nor an executable file '{path}'"
    )


def execute(path, *extra_command_args):
  """
  If ``path`` is an executable file, execute it; if ``path`` is a
  directory, execute all executables in there.

  In any case, ``extra_command_args`` will be handed to the executed
  files.

  Raises CalledProcessError
  """
  if isdir(path):

    call_args = ['run-parts', '--exit-on-error', '--regex', '.*']
    if getLogger().isEnabledFor(DEBUG):
      call_args.append('-v')
    for extra_arg in extra_command_args:
      call_args.extend(('-a', extra_arg))
    call_args.append(path)

  else:
    call_args = (path,) + extra_command_args

  debug("executing: %s", call_args)

  run(call_args, check=True)
