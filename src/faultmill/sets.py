"""
Module contains some classes that behave like built-in sets but feature
some helpers.
"""

from itertools import combinations, chain


class SetHelpersMixin:
  """
  A few helpers for sets.
  """

  def all_possible_subsets_iter(self, min_cardinality, max_cardinality):
    """
    Returns an interable containing all possible subsets of
    ``source_set`` with the corresponding min/max cardinalities.
    """
    self_cls = self.__class__
    cardinalities = range(min_cardinality, max_cardinality+1)
    combinations_all_cardinalities = chain.from_iterable(
      combinations(self, c) for c in cardinalities
    )
    return (self_cls(i) for i in combinations_all_cardinalities)

  def powerset_iter(self):
    """
    Returns an interable containing all possible subsets of
    ``source_set``.
    """
    return self.all_possible_subsets_iter(0, len(self))

  def true_for_any(self, test_func, elements, exclude_self=False):
    """
    Returns ``True`` if ``test_func`` evaluates to ``True`` for any element
    in ``elements``, ``False`` otherwise.
    Elements specified by ``exclude`` will be ignored and not tested.
    """
    # note: avoid performance killers (like making copies of objects) here
    for element in elements:
      if exclude_self and element is self:
        continue
      if test_func(element):
        return True
    return False

  def issuperset_any(self, other_sets, exclude_self=False):
    """
    Returns ``True`` if ``self`` is a superset of any ``other_sets``,
    ``False`` otherwise.
    If ``exclude_self`` is ``True``, ``self`` will not be counted
    as a superset of itself. IOW, ``self`` it will be ignored.
    """
    return self.true_for_any(self.issuperset, other_sets, exclude_self)

  def issubset_any(self, other_sets, exclude_self=False):
    """
    Analog to ``is_superset_of_any``.
    """
    return self.true_for_any(self.issubset, other_sets, exclude_self)


class Set(set, SetHelpersMixin):
  """ Just like ``set`` but with helpers. """

  def __repr__(self):
    return f"{{{', '.join(repr(e) for e in self)}}}"


class StaticSet(frozenset, SetHelpersMixin):
  """ Like ``frozenset`` but with helpers and it prints more condensed. """

  def __repr__(self):
    return f"{{{', '.join(repr(e) for e in self)}}}ˢ"
