"""
This module contains the abstractions for the syntax elements of a
fault tree. It is hence the main place where such semantics, invariants
etc. are encoded.
"""

from abc import ABCMeta, abstractmethod
from itertools import product, chain
from logging import debug

from faultmill.exceptions import TreeError
from faultmill.sets import Set, StaticSet
from faultmill.transformations import ImplicationRedundancyRemoval


class TreeElement(metaclass=ABCMeta):
  """
  Anything that is part of a tree.
  For historic reasons also commonly referred to as 'node'.
  """

  ALLOWED_PARENT_TYPES = ("TreeElement", )
  """ Classes (and subclasses thereof) that are allowed as parents. """

  MAX_PARENTS = 1
  """ Maximum number of children allowed. Negative values mean no limit. """

  ALLOWED_CHILD_TYPES = ("TreeElement", )
  """ Classes (and subclasses thereof) that are allowed as children. """

  MAX_CHILDREN = -1
  """ Maximum number of children allowed. Negative values mean no limit. """

  def __init__(self, name):
    """
    Initializes instance variables.
    """
    assert isinstance(name, str)
    self.name = name
    self.parents = Set()
    self.children = Set()

  def _add_relative(self, to_add, add_to, allowed_types, allowed_count):
    """
    Used to check ``ALLOWED_…_TYPES`` and ``MAX_…``.
    Raises ``TreeError`` when violated.

    More specifically, if ``to_add`` is any of ``allowed_types`` and
    ``add_to`` has not reached ``allowed_count`` yet; add ``to_add`` to
    ``add_to``.
    """

    friendly_relation_name = 'child' if to_add == self else 'parent'
    errmsg_prefix = (
      f'Cannot add {to_add} as {friendly_relation_name} of {self}: '
    )

    count_is_limited = allowed_count >= 0
    count_is_exceeded = len(add_to) >= allowed_count
    if count_is_limited and count_is_exceeded:
      raise TreeError(
        errmsg_prefix +
        f'already maximum of {allowed_count} ' +
        f'{friendly_relation_name} elements attached'
      )

    globals_ = globals()
    allowed_types_classes = (globals_[t] for t in allowed_types)
    if not any(isinstance(to_add, t) for t in allowed_types_classes):
      raise TreeError(
        errmsg_prefix +
        f'not any of the allowed types ({allowed_types})'
      )

    add_to.add(to_add)

  def _add_parent(self, parent):
    """
    Adds ``parent`` as a parent of ``self``.

    Does not establish the reverse relation (``self`` as child of
    ``parent``). You should only (need to) use ``wire_child`` to
    assemble trees which *does* case about the reverse relations
    instead.
    """
    self._add_relative(parent, self.parents, self.ALLOWED_PARENT_TYPES,
                       self.MAX_PARENTS)

  def _add_child(self, child):
    """
    Adds ``child`` as a child of ``self``.

    Does not establish the reverse relation (``self`` as parent of
    ``child``). You should only (need to) use ``wire_child`` to
    assemble trees which *does* case about the reverse relations
    instead.
    """
    self._add_relative(child, self.children, self.ALLOWED_CHILD_TYPES,
                       self.MAX_CHILDREN)

  def wire_child(self, child):
    """
    Adds ``child`` as child of ``self`` and also ``self`` as parent of
    ``child``.
    """
    self._add_child(child)

    # We made the ``_add_…`` protected to make clear that calling this
    # method incautiously might lead to an inconsistent internal state.
    # However, at some point the method *has* to be called from outside
    # the instance. This is the only point where we do this.
    # pylint: disable=protected-access
    child._add_parent(self)

  @abstractmethod
  def cutsets(self):
    """
    Returns an set of all cutsets of this node (recursively).
    The returned sets are composed of basic events.

    The calculation happens very similar to MOCUS ("Method for Obtaining
    Minimal Cut Sets") but recursively instead of iteratively.
    In analogy to MOCUS, the returned tuple can be seen as a column
    and the sets therein as rows.
    """

  def transformations(self):
    """
    Returns transformations to apply after the cutsets have been
    determined.

    Subclasses might override this method which returns an empty set.

    E.g., this is useful for instances of ``TreeElement`` to
    communicate which redundancies can be removed that they introduced
    during the determination of cutsets (FDEPs).
    """
    return tuple()

  def __repr__(self):
    if self.name:
      return self.name
    return super().__repr__()


class IntermediateEvent(TreeElement):
  """
  Represents an intermediate event of a fault tree.
  """

  def cutsets(self):
    return next(iter(self.children)).cutsets()


class TopEvent(IntermediateEvent):
  """
  Represents the root event of a fault tree.
  """

  def cutsets(self):
    return next(iter(self.children)).cutsets()


class BasicEvent(TreeElement):
  """
  Represents a basic event of a fault tree.

  Although basic events do not require much (i.e., "any") logic
  normally, it's a different story when functional dependencies (FDEPs)
  are attached. This class contains the logic for the latter case.
  """

  ALLOWED_PARENT_TYPES = ("Gate", "FDEP", "TopEvent")
  ALLOWED_CHILD_TYPES = ("FDEP",)

  def fdep(self):
    """ Returns the FDEP attached to ``self`` or ``None``. """
    children = self.children
    if not children:
      return None
    fdep = next(iter(children))
    if not fdep:
      return None
    assert isinstance(fdep, FDEP), "child of BasicEvent can only be FDEP"
    return fdep

  def cutsets(self):
    """
    For ordinary basic events, the only combination that triggers the
    event (the event itself) is returned.

    If, however, the basic event has an functional dependency (FDEP)
    attached to it, the event itself and the FDEP (effectively the
    sub-tree attached to the FDEP) are ORed and the cutset of that OR
    gate is returned.
    To remove the introduced redundancy, a corresponding transformation
    is being created.
    """

    children = self.children

    if not self.children:
      # This is basically the recursion termination.
      # With MOCUS in mind, we return an element within a row within a
      # column.
      return StaticSet((StaticSet((self,)),))

    fdep = self.fdep()

    fdep_or = OrGate(f"OR for combination {((self, fdep),)}")
    debug(f"{self} connected to FDEP, transformed to '{fdep_or}'")

    # temporarily remove child in order to be able to get the cutset of
    # myself (note: creating a copy of self is no option since it
    # would destroy comparisons based on object identity)
    self.children = Set()

    fdep_or.children.add(self)
    fdep_or.children.add(fdep)
    cutsets = fdep_or.cutsets()

    # restore self's original children
    self.children = children

    debug("cutsets of '%s': %s", self, cutsets)
    return cutsets

  def transformations(self):
    fdep = self.fdep()
    if not fdep:
      return StaticSet()

    return (ImplicationRedundancyRemoval(cutset, fdep.parents)
            for cutset in fdep.cutsets())


class FDEP(TreeElement):
  """
  Represents a functional dependency (FDEP) gate of a fault tree.

  This class contains surprisingly little logic since the elements where
  FDEPs are attached to (namely basic events) have to implement this.
  """

  ALLOWED_PARENT_TYPES = ("BasicEvent", )
  MAX_PARENTS = -1

  ALLOWED_CHILD_TYPES = ("BasicEvent", "Gate")
  MAX_CHILDREN = 1

  def cutsets(self):
    """
    Note: specifics of FDEPs handled by instances of ``BasicEvent``.
    """
    cutsets = next(iter(self.children)).cutsets()
    debug("cutsets of '%s': %s", self, cutsets)
    return cutsets


# pylint is not smart enough at the moment to detect half-implemented
# abstract classes
# (https://github.com/PyCQA/pylint/issues/179#issuecomment-403783079)
# pylint: disable=abstract-method
class Gate(TreeElement, metaclass=ABCMeta):
  """
  An abstract base class for all classes representing a type of a fault
  tree gate.
  """

  ALLOWED_PARENT_TYPES = ("IntermediateEvent", "FDEP")
  MAX_PARENTS = 1

  ALLOWED_CHILD_TYPES = ("IntermediateEvent", "BasicEvent")
  MAX_CHILDREN = -1

  def _check_presence_of_children(self):
    """
    Raises a ``TreeError`` if there are no children attached to this
    gate.
    """
    if not self.children:
      raise TreeError(
        f"{self} is supposed to have children, but it doesn't."
      )

  def children_cutsets_iter(self):
    """
    Returns (an iterable of iterables of) the children's cutsets.
    """
    return (c.cutsets() for c in self.children)


class AndGate(Gate):
  """
  Represents an *and* gate of a fault tree.
  """

  def cutsets(self):
    self._check_presence_of_children()
    column_products = product(*self.children_cutsets_iter())

    # Since ``itertools.product`` generates tuples of tuples (i.e.
    # all combinations of the input tuples), we now have tuples of tuples
    # as "rows". Hence, we have to flatten them to one-dimensional sets.
    # IOW, since ``itertools.product`` doesn't know that a combination of
    # tuples in a row (e.g. ``((1,2), (3,4))``) is the same as both tuples
    # chained (in our case), we have to chain them manually:
    out_column = StaticSet(
      StaticSet(chain.from_iterable(row_tuples)) for row_tuples in column_products
    )
    debug("cutsets of '%s': %s", self, out_column)
    return out_column


class OrGate(Gate):
  """
  Represents an *or* gate of a fault tree.
  """

  def cutsets(self):
    self._check_presence_of_children()
    stacked_cloumn = StaticSet(
      chain.from_iterable(
        self.children_cutsets_iter()
      )
    )
    debug("cutsets of '%s': %s", self, stacked_cloumn)
    return stacked_cloumn


class VotingOrGate(Gate):
  """
  Represents a *voting or* gate of a fault tree (where "n out of m"
  fired child elements make this gate fire).
  """

  def __init__(self, name, vote_count):
    super().__init__(name)
    self.vote_count = vote_count

  def cutsets(self):
    """
    Creates a temporary tree of ANDed ORs of all triggering event
    combinations and uses the resulting tree to generate its output.
    """
    # calculate what is needed to build the temporary tree
    children = self.children
    triggering_event_combinations_iter = (
      children.all_possible_subsets_iter(self.vote_count, len(children))
    )

    # build temporary tree with an OR and ANDs only:
    debug("building temporary tree for %s", self)
    tmp_root = OrGate("top-level OR")
    for i, combination in enumerate(triggering_event_combinations_iter):
      # since we are re-using tree elements, we write directly to the
      # set of parents/children to bypass the sanity checks that are
      # designed to validate user-provided trees
      and_gate = AndGate(f"AND for combination {i}")
      tmp_root.children.add(and_gate)
      for event in combination:
        debug("adding to %s: %s", and_gate, event)
        and_gate.children.add(event)

    out_column = tmp_root.cutsets()
    debug("cutsets of '%s': %s", self, out_column)
    return out_column
