"""
This module is contains proxy-style classes to graph data structures
parsed from source files of different formats.
Communication between the classes herein and callers is based on IDs.
"""

from abc import ABCMeta, abstractmethod
from logging import debug

from faultmill.exceptions import TreeError
from faultmill.tree_elements import (TopEvent, IntermediateEvent, BasicEvent,
                                     FDEP, AndGate, OrGate, VotingOrGate)
from faultmill.utils import re_compile_ci


class SourceTypeRegistry(metaclass=ABCMeta):
  """
  All the concrete subclasses of ``GraphSource`` have to register here
  with a key(word). The key has to be provided on the CLI to specify
  the format of a graph file.
  """
  types = {}

  @classmethod
  def register(cls, key):
    """
    A decorator factory, returning the decorator for ``key``.
    """
    def wrap(model_cls):
      cls.types[key] = model_cls
      return model_cls
    return wrap

  @classmethod
  def keys(cls):
    """
    Return all registered graph sources (i.e., the key(words) they
    registered with).
    """
    return cls.types.keys()

  @classmethod
  def type_for_key(cls, key):
    """
    Returns the type registered for the specified ``key``.
    """
    return cls.types[key]


class GraphSource(metaclass=ABCMeta):
  """
  Abstracts between different graph sources (e.g. file types).
  This layer of abstraction is not aware of trees and their invariants.
  """

  def __init__(self, file_path):
    """
    Responsible for loading the graph from the source (i.e., from file).
    """
    debug("Loading %s '%s'", self.__class__.__name__, file_path)
    self.file_path = file_path

  @abstractmethod
  def initialized_nodes(self, node_id):
    """
    Determines and returns all possible/valid graph elements the element
    for node with ``node_id``.
    (There is yet no known case where more than one interpretation is
    valid but this method allows ``GraphSource`` to unify the check for
    exactly one interpretation.)
    """

  def initialized_node(self, node_id):
    """
    Same as ``initialized_nodes`` but raises an error when more than
    exactly one interpretation is found.
    """
    nodes = self.initialized_nodes(node_id)
    if len(nodes) != 1:
      raise TreeError("Could not identify exactly one node type for " +
                      f"node '{node_id}'. Identified: {nodes}.")
    return next(iter(nodes))

  @abstractmethod
  def directed(self):
    """
    Returns whether the underlying graph is directed.
    """

  @abstractmethod
  def node_ids_iter(self):
    """
    Returns iterator for node IDs.
    """

  def root_nodes_ids(self):
    """
    Returns the IDs of root nodes (i.e. nodes without a parent).
    """
    return tuple()

  def root_node_id(self):
    """
    Same as ``root_nodes_ids`` but raises an error when more than one
    root node is found.
    """
    roots = self.root_nodes_ids()
    if len(roots) != 1:
      raise TreeError(
        f"{self} does not have exactly one root node. Got: {roots}"
      )
    return next(iter(roots))

  @abstractmethod
  def edge_tuples_iter(self):
    """
    Returns iterable of tuples: (source node ID, target node ID).
    """

  def __repr__(self):
    return f"{self.__class__.__name__}('{self.file_path}')"

  def __str__(self):
    return repr(self)


@SourceTypeRegistry.register("dot")
class DotSource(GraphSource):
  """
  Reads fault trees from dot files.

  Since dot files have no native syntax for fault trees (and there is
  no convention to express fault trees in dot); this class introduces a
  convention to express fault trees in dot (see ``SIMPLE_NODE_TYPES``
  and ``initialized_nodes``).
  """

  SIMPLE_NODE_TYPES = (
    (re_compile_ci(r'^r(oot)?(_.*)?$'), TopEvent),
    (re_compile_ci(r'^i(nter)?(_.*)?$'), IntermediateEvent),
    (re_compile_ci(r'^b(asic)?(_.*)?$'), BasicEvent),
    (re_compile_ci(r'^a(nd)?(_.*)?$'), AndGate),
    (re_compile_ci(r'^o(r)?(_.*)?$'), OrGate),
    (re_compile_ci(r'^f(dep)?(_.*)?$'), FDEP),
  )
  """ A tuple of tuples of regular expressions to detect element types
  mapped to a function responsible for initializing the graph element. """

  def __init__(self, dotfile_path):
    super().__init__(dotfile_path)

    # if users do not use GraphML, we don't want to force the library to
    # be present, so we import it here just in time; hence:
    # pylint: disable=import-outside-toplevel
    from pygraphviz import AGraph

    self.graph = AGraph(dotfile_path)

  def initialized_nodes(self, node_id):
    debug("initializing element '%s'", node_id)

    # we first initialize matching node types not requiring special
    # arguments ...
    nodes = [node_type(node_id)
             for node_id_regexp, node_type in self.SIMPLE_NODE_TYPES
             if node_id_regexp.match(node_id)]

    # ... and then the ones that do require special treatment:
    match = re_compile_ci(r'^vote_([0-9]+)(_.*)?$').match(node_id)
    if match:
      nodes.append(
        VotingOrGate(
          node_id,
          int(match.group(1)))
      )

    debug("%s interpreted as %r", node_id, nodes)
    return nodes

  def directed(self):
    return self.graph.directed

  def node_ids_iter(self):
    return (node.name for node in self.graph.nodes_iter())

  def root_nodes_ids(self):
    return tuple(node_id for node_id in self.node_ids_iter()
                 if not self.graph.predecessors(node_id))

  def edge_tuples_iter(self):
    return ((node.name for node in edge_tuple)
            for edge_tuple in self.graph.edges_iter())


@SourceTypeRegistry.register("FuzzEdGraphMl")
class FuzzEdGraphMLSource(GraphSource):
  """
  Reads fault trees from FuzzEd (now The Open Reliability Editor) XML
  exoprts.

  Keep in mind that this program does not support all syntax elements
  yet; especially no timing-dependent semantics.
  """

  IGNORED_ELEMENTS = ("stickyNote", )

  SIMPLE_NODE_TYPES = (
    ("topEvent", TopEvent),
    ("basicEvent", BasicEvent),
    ("intermediateEvent", IntermediateEvent),
    ("undevelopedEvent", IntermediateEvent),
    ("andGate", AndGate),
    ("orGate", OrGate),
    ("fdepGate", FDEP),
  )
  """ A tuple of tuples of "kinds" (as used by FuzzEd) to detect node types
  mapped to a function responsible for initializing the graph element. """

  @staticmethod
  def node_attr_value(node, attr):
    """
    Shorcut to return the value of the ``attr`` of ``node``; ``None`` if
    ``attr`` is unavailable.
    """
    if attr not in node.attr:
      return None
    return node.attr[attr].value

  @classmethod
  def _node_id(cls, graph_node):
    return str(graph_node.id)

  @classmethod
  def _node_name(cls, graph_node):
    return (cls.node_attr_value(graph_node, "name") or
            cls._node_id(graph_node))

  def __init__(self, file_path):
    super().__init__(file_path)

    # if users do not need the to parse GraphML, we don't want to force
    # the library to be present, so we import it here just in time;
    # hence: pylint: disable=import-outside-toplevel
    from pygraphml import GraphMLParser

    self.graph = GraphMLParser().parse(file_path)

    # map non-ignored node IDs to nodes
    self._nodes = {
      self._node_id(node): node
      for node in self.graph.nodes()
      if self.node_attr_value(node, "kind") not in self.IGNORED_ELEMENTS
    }

  def initialized_nodes(self, node_id):
    source_node = self._nodes[node_id]
    source_node_kind = self.node_attr_value(source_node, "kind")

    # first initialize node types/kinds not requiring special arguments:
    out_nodes = [node_type(self._node_name(source_node))
                 for node_kind, node_type in self.SIMPLE_NODE_TYPES
                 if node_kind == source_node_kind]

    # ... and then the ones that do require special arguments:
    if source_node_kind == "votingOrGate":
      out_nodes.append(
        VotingOrGate(
          node_id,
          int(self.node_attr_value(source_node, "k"))
        )
      )

    debug("%s interpreted as %r", node_id, out_nodes)
    return out_nodes

  def directed(self):
    return self.graph.directed

  def node_ids_iter(self):
    return iter(self._nodes)

  def root_nodes_ids(self):
    root_nodes_ids = [node_id
                      for node_id, node in self._nodes.items()
                      if not node.parent()]

    root_from_graph_attr = self.graph.root()
    if root_from_graph_attr:
      root_nodes_ids.append(root_from_graph_attr)

    return root_nodes_ids

  def edge_tuples_iter(self):
    return ((self._node_id(edge.parent()), self._node_id(edge.child()))
            for edge in self.graph.edges())
