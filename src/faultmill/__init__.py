"""
This CLI applications reads a fault tree from a file and extracts maximum
fault injection campaigns.
A campaign is a set of fault injection scenarios.
A scenario is a set of basic events so that the root event does not
trigger.
A scenario is maximum, iff it is not a subset of any other possible
scenario.
A campaign is maximum, iff it contains all maximum scenarios.

For the naming conventions regarding DOT files, please refer to the
examples.
"""

import sys
import argparse
from logging import getLogger, DEBUG, INFO
from itertools import chain

from faultmill.tree import FaultTree
from faultmill.sources import SourceTypeRegistry
from faultmill.runnable import Campaign


def standalone():
  """
  CLI entry point and top-level program coordination.
  """
  parser = argparse.ArgumentParser(
    description=__doc__
  )

  parser.add_argument('-d', '--debug', action='store_true',
                      default=False, help='turn on debug messages')
  parser.add_argument('-v', '--verbose', action='store_true',
                      default=False, help='turn on verbose messages')
  parser.add_argument('-w', '--workers',
                      help=('run the campaign with the workers in ' +
                            'this directory'))
  parser.add_argument('-b', '--basic-events', action='store_true',
                      help=('list the basic events required for '+
                            'running the campaign'))
  parser.add_argument('-s', '--serial', action='store_true',
                      help=('run event workers in serial instead of '
                            'parallel'))
  parser.add_argument('-l', '--limit', nargs='*', default=None,
                      help=('limit the campaign to be run to these ' +
                            'scenarios (provide their hashes)'))
  parser.add_argument('-n', '--runs', type=int, default=1,
                      help=('run every scenario this many times' +
                            '(negative numbers will let them repeat '
                            'forever)'))
  parser.add_argument('-r', '--retries', type=int, default=0,
                      help=('retry run but failed scenarios this many ' +
                            'times (negative numbers mean no limit)'))
  parser.add_argument('file_format', choices=SourceTypeRegistry.keys(),
                      help='format of file specified by file_path')
  parser.add_argument('file_path',
                      help='file to read the fault tree from')

  args = parser.parse_args()

  # set up logger
  getLogger().name = ""
  if args.verbose:
    getLogger().setLevel(INFO)
  if args.debug:
    getLogger().setLevel(DEBUG)

  source_type = SourceTypeRegistry.type_for_key(args.file_format)
  source = source_type(args.file_path)
  tree = FaultTree(source)
  injection_sets = tree.maximum_injection_sets()

  if args.basic_events:
    for event in set(chain.from_iterable(injection_sets)):
      print(event)
    sys.exit(0)

  if args.workers:
    campaign = Campaign(args.workers, injection_sets, args.retries)
    campaign.do_pre_run_checks()
    campaign.run(args.serial, args.limit, args.runs)
  else:
    campaign = Campaign("dummy", injection_sets, args.retries)
    scenarios = campaign.scenarios
    print(f"{campaign} with {len(scenarios)} scenarios")
    for scenario in scenarios:
      print(f"  {scenarios}")
      for event in scenario.events:
        print(f"    {event}")
