"""
This module contains abstractions which can be run (as in "executed").
Namely, campaigns and scenarios.
"""

from abc import ABCMeta, abstractmethod
from concurrent.futures import ThreadPoolExecutor
from hashlib import md5
from logging import info, error, warning
from os.path import join as path_join
from subprocess import CalledProcessError

from faultmill.utils import check_executable, execute


class Runnable(metaclass=ABCMeta):
  """
  Common base class for classes which can be executed/run.

  It mainly serves the purpose of ensuring a common interface and being
  able to identify instances by a digest. The digest is (intended to be)
  unique and stable between executions of this tool. It can hence be
  used to identify scenarios and campaigns.
  """

  _DIGEST_MASK = ([False] * 7 + [True]) * 4

  @abstractmethod
  def do_pre_run_checks(self):
    """
    Can do few pre-flight checks to catch some trivial setup problems
    (of this program, e.g., missing directories this program requires)
    prior to calling ``run()``.
    """

  @abstractmethod
  def run(self, serial=False):
    """
    Well… responsible for running the runnable; does whatever has to be
    done.

    If not ``serial`` (the flag is probably most useful for debugging),
    sub classes might run things concurrently.
    """

  @abstractmethod
  def _digest_strings(self):
    """
    Returns an iterable of strings to derive the ``self.digest()`` from.

    Sub classes should choose this wisely in order to make
    ``self.digest()`` work as intended (see class' and ``self.digest()``
    documentation). Specifically, the strings should identify the
    instance (not as in "object in memory" but as in "semantics of the
    runnable") uniquely and should be stable across program executions.
    """

  def digest(self):
    """
    Returns a digest of four characters derived from a list of strings.
    """
    strings = self._digest_strings()
    joined = ','.join(sorted(strings))
    full_hash = md5(joined.encode()).hexdigest()
    return ''.join(
      hash_bit
      for hash_bit, mask_bit in zip(full_hash, self._DIGEST_MASK)
      if mask_bit
    )

  def __str__(self):
    return f"{self.__class__.__name__} {self.digest()}"


class Campaign(Runnable):
  """
  Just like ``Scenario`` but also runs workers prior and after
  running all scenarios.
  """

  PRE_CAMPAIGN_DIR = "pre_campaign"
  POST_CAMPAIGN_DIR = "post_campaign"

  def __init__(self, workers_dir, injection_sets, retry_count):
    """
    Initializes a runnable ``Campaign`` from ``injection_sets`` and
    with the worker scripts in ``workers_dir``.
    Failed scenarios will be retried ``retry_count`` times.
    """
    self.pre_workers_dir = path_join(workers_dir, self.PRE_CAMPAIGN_DIR)
    self.post_workers_dir = path_join(workers_dir, self.POST_CAMPAIGN_DIR)
    self.scenarios = [Scenario(workers_dir, injection_set)
                      for injection_set in injection_sets]
    self.retry_count = retry_count

  def do_pre_run_checks(self):
    check_executable(self.pre_workers_dir, self.post_workers_dir)
    for scenario in self.scenarios:
      scenario.do_pre_run_checks()

  # I don't agree with pylint here, which states introducing optional
  # arguments is a smell (https://github.com/PyCQA/pylint/issues/431):
  # pylint: disable=arguments-differ
  def run(self, serial=False, scenario_digests=None, runs=1):
    """
    Runs scenarios of this campaign ``runs`` times.
    If ``serial``, the faults are injected in series rather than parallel
    (probably only useful for debugging).
    ``scenario_digests`` might be provided as a list of scenario hashes
    (strings) to be run. If ``scenario_digests`` is ``None``, all scenarios
    are run.
    """
    info("running %s", self)

    # docstrings for variables follow, so
    # pylint: disable=pointless-string-statement,

    scenarios = [s for s in self.scenarios
                 if scenario_digests is None
                 or s.digest() in scenario_digests]
    """
    The scenarios to run (filtered if hashes were supplied).
    This list will be filtered during the execution of the campaign,
    removing scenarios that have no runs or no retries left.
    """

    runs_left = {s: runs for s in scenarios}
    """ A countdown "runs still to do" per scenario. """

    retries_left = {s: self.retry_count for s in scenarios}
    """ A countdown "retries left for failed runs" per scenario. """

    runs_total = sum(runs_left.values())

    # we catch keyboard interrupts from now on:
    try:
      execute(self.pre_workers_dir)

      while scenarios:
        remaining_scenarios = []
        for scenario in scenarios:

          retries = retries_left.get(scenario)
          runs = runs_left.get(scenario)

          # no defensive checking (``runs < 0``) here to allow an infinite
          # number of runs
          if runs == 0:
            continue

          runs_left_sum = sum(runs_left.values())
          info("%i of %i (%i%%) scenario runs left to do in total",
               runs_left_sum, runs_total, 100*runs_left_sum/runs_total)

          try:
            scenario.run(serial)
          except CalledProcessError:
            warning("%s failed!", scenario)

            # no defensive checking (``retries < 0``) here to allow an
            # infinite number of retries (i.e. negative values)
            if retries == 0:
              continue

            retries -= 1
          else:
            runs -= 1

          info("%s: runs left: %i, retries left: %i", scenario, runs,
               retries)

          remaining_scenarios.append(scenario)
          retries_left[scenario] = retries
          runs_left[scenario] = runs

        scenarios = remaining_scenarios

    except KeyboardInterrupt:
      info("keyboard interrupt - running post campaign workers")
    finally:
      execute(self.post_workers_dir)

    info("finished running %s", self)

    for scenario in (s for s, runs in runs_left.items() if runs > 0):
      assert (retries_left[scenario] > 0) == (runs_left[scenario] == 0)
      assert (retries_left[scenario] <= 0) == (runs_left[scenario] >= 0)
      error("not all runs done (retry limit of %i reached): %s",
            self.retry_count, scenario)

  def _digest_strings(self):
    return (scenario.digest() for scenario in self.scenarios)


class Scenario(Runnable):
  """
  Represents a set of events that can be triggered using workers.
  Additional workers prior and after running the scenario are supported
  as well.
  """

  PRE_SCENARIO_DIR = "pre_scenario"
  EVENT_WORKERS_DIR = "event"
  POST_SCENARIO_DIR = "post_scenario"

  def __init__(self, workers_dir, events):
    """
    Takes an iterable of event names to trigger.
    """
    self.pre_workers_dir = path_join(workers_dir, self.PRE_SCENARIO_DIR)
    self.event_workers_dir = path_join(workers_dir, self.EVENT_WORKERS_DIR)
    self.post_workers_dir = path_join(workers_dir, self.POST_SCENARIO_DIR)
    self.events = events

  def _digest_strings(self):
    return (event.name for event in self.events)

  def do_pre_run_checks(self):
    check_executable(self.pre_workers_dir, self.event_workers_dir,
                     self.post_workers_dir)

  def run(self, serial=False):
    info("running %s", self)

    execute(self.pre_workers_dir, self.digest())

    thread_count = 1 if serial else len(self.events)
    with ThreadPoolExecutor(max_workers=thread_count) as executor:

      def submit(name):
        ''' shortcut for readability '''
        return executor.submit(
          execute,
          self.event_workers_dir,
          self.digest(),
          name
        )

      # we do not use a generator here since we do *not* want the next
      # line to be lazy (submissions should be timely as close together
      # as possible)
      futures = [submit(event.name) for event in self.events]

      # read futures' results to re-raise any exceptions that maybe
      # occurred in the threads
      for future in futures:
        future.result()

    execute(self.post_workers_dir, self.digest())
